{
    "id": "4166f9d0-1f67-4b73-b246-484dbd5a0fcc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_controller",
    "eventList": [
        {
            "id": "4c4eec74-fb54-4ae2-a1e5-84ce1d8c28aa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4166f9d0-1f67-4b73-b246-484dbd5a0fcc"
        },
        {
            "id": "9d39e5e3-44c1-4980-b224-a8c78b3c4f90",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "4166f9d0-1f67-4b73-b246-484dbd5a0fcc"
        },
        {
            "id": "6082f23c-fd77-41ea-885e-b3d222320da6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4166f9d0-1f67-4b73-b246-484dbd5a0fcc"
        },
        {
            "id": "7e6eda56-1346-4186-9241-4ad0cf7fdfd6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 10,
            "m_owner": "4166f9d0-1f67-4b73-b246-484dbd5a0fcc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}