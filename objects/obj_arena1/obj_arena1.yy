{
    "id": "9717c734-9342-456c-b465-ae9bbfd7b7e7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_arena1",
    "eventList": [
        {
            "id": "2f667fb5-3276-4a02-9f06-553a41f4a1b3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "9717c734-9342-456c-b465-ae9bbfd7b7e7"
        },
        {
            "id": "0a1fa7a7-0488-4a03-b471-2f293df10229",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9717c734-9342-456c-b465-ae9bbfd7b7e7"
        },
        {
            "id": "23dc4a48-9d6a-4786-b8ab-9bdf6bcc0ff6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "9717c734-9342-456c-b465-ae9bbfd7b7e7"
        },
        {
            "id": "80b7a045-b0fe-4dac-bf90-023b12a65b71",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "9717c734-9342-456c-b465-ae9bbfd7b7e7"
        },
        {
            "id": "6cc6e2da-1fff-4e4d-933d-c29409796e90",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "9717c734-9342-456c-b465-ae9bbfd7b7e7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2cf09e49-20f3-47fa-8b45-195959d0ca58",
    "visible": true
}