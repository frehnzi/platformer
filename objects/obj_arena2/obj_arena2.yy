{
    "id": "a1dac66f-6479-4134-a58e-9307c9ca2b92",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_arena2",
    "eventList": [
        {
            "id": "0232229d-e182-41b3-bcfb-0807b93be42e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "a1dac66f-6479-4134-a58e-9307c9ca2b92"
        },
        {
            "id": "2f65a2f6-651d-4d5b-9db0-4a528446f28b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a1dac66f-6479-4134-a58e-9307c9ca2b92"
        },
        {
            "id": "38934d22-3463-4e16-a5a0-926e2ab907e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "a1dac66f-6479-4134-a58e-9307c9ca2b92"
        },
        {
            "id": "197045e1-a33c-423a-ba05-9eb02cc874c8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "a1dac66f-6479-4134-a58e-9307c9ca2b92"
        },
        {
            "id": "60523673-3cd2-419e-9bb4-f591e9ff8004",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "a1dac66f-6479-4134-a58e-9307c9ca2b92"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "58c73656-ac3f-471b-b7bc-fa6518281fc9",
    "visible": true
}