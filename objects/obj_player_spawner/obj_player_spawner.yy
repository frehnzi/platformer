{
    "id": "39d2d488-cc74-4302-bcb7-6183cbbd8405",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player_spawner",
    "eventList": [
        {
            "id": "345f5b68-a5e0-4208-874a-b752e1384502",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "39d2d488-cc74-4302-bcb7-6183cbbd8405"
        },
        {
            "id": "31cbc13f-7194-4afd-99ec-e9b0819d2165",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "39d2d488-cc74-4302-bcb7-6183cbbd8405"
        },
        {
            "id": "d03e4221-50d5-4e9a-91bc-07fdc85cd41f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "39d2d488-cc74-4302-bcb7-6183cbbd8405"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}