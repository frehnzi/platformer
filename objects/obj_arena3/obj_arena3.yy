{
    "id": "cee031f5-b8d2-49f3-bc47-41cd831af9fd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_arena3",
    "eventList": [
        {
            "id": "1bc72dd6-ec00-48b7-b192-ea1defc9bf53",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "cee031f5-b8d2-49f3-bc47-41cd831af9fd"
        },
        {
            "id": "29f0927e-ec4c-422d-ae1d-369a37efd280",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cee031f5-b8d2-49f3-bc47-41cd831af9fd"
        },
        {
            "id": "14d427c0-d64f-4019-a223-aa7843f67c1f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "cee031f5-b8d2-49f3-bc47-41cd831af9fd"
        },
        {
            "id": "a53f0fc2-124d-4c57-82ee-6152ca3c695d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "cee031f5-b8d2-49f3-bc47-41cd831af9fd"
        },
        {
            "id": "b7942bc0-db8a-4c8c-ad96-66a26a28ba33",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "cee031f5-b8d2-49f3-bc47-41cd831af9fd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "40d65e0f-a903-47cd-8148-eaeed087dadf",
    "visible": true
}