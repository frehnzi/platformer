{
    "id": "e76ee4bb-f216-4d80-b751-a4926fce5d5f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player1",
    "eventList": [
        {
            "id": "f1524b44-41b0-4dbc-b753-4d77192b4d1f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e76ee4bb-f216-4d80-b751-a4926fce5d5f"
        },
        {
            "id": "24a8da8d-fcc4-40b0-bdcf-ac072e4ff3fc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e76ee4bb-f216-4d80-b751-a4926fce5d5f"
        },
        {
            "id": "189947cb-016b-4aeb-b87b-b06627136345",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 7,
            "m_owner": "e76ee4bb-f216-4d80-b751-a4926fce5d5f"
        },
        {
            "id": "0adf0aa8-1932-4ea1-a538-262dd7fd666c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "4fdd9e4a-ceab-46ca-906c-5a617ffe0774",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e76ee4bb-f216-4d80-b751-a4926fce5d5f"
        },
        {
            "id": "a71eab26-ecf7-4838-a696-f42a6657af3e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "e76ee4bb-f216-4d80-b751-a4926fce5d5f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fdae3fae-e36a-4691-bb78-f74dcc9b8be2",
    "visible": true
}