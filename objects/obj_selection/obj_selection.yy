{
    "id": "24bdc9ff-d6a8-4c2e-baca-f0c7b6df411c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_selection",
    "eventList": [
        {
            "id": "fbb119d9-5248-401c-90a6-738c90d5f751",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "24bdc9ff-d6a8-4c2e-baca-f0c7b6df411c"
        },
        {
            "id": "de99481e-adac-4130-8bed-73b7e89c53b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "24bdc9ff-d6a8-4c2e-baca-f0c7b6df411c"
        },
        {
            "id": "f9007b98-2d4b-4142-a2f6-7d4a9a01499e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "24bdc9ff-d6a8-4c2e-baca-f0c7b6df411c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8d7c34fc-9812-450e-ac86-df46467d9895",
    "visible": true
}