{
    "id": "6090d934-9d7f-47d6-8da9-340a8c1aacd1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_playerdeath1",
    "eventList": [
        {
            "id": "8d5038b8-8eba-499a-bc98-58c6f9b437ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6090d934-9d7f-47d6-8da9-340a8c1aacd1"
        },
        {
            "id": "8943ac2a-914c-4dff-8722-cbd848b28631",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "4dafb8de-74e4-4263-b79a-0f3d9b26d1ba",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6090d934-9d7f-47d6-8da9-340a8c1aacd1"
        },
        {
            "id": "3734e58f-399d-48f6-8b15-3517e5fa11f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "6090d934-9d7f-47d6-8da9-340a8c1aacd1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fdae3fae-e36a-4691-bb78-f74dcc9b8be2",
    "visible": true
}