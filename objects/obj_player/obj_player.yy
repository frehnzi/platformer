{
    "id": "4fdd9e4a-ceab-46ca-906c-5a617ffe0774",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "efdd04c9-5566-4587-b484-50071c732bf4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4fdd9e4a-ceab-46ca-906c-5a617ffe0774"
        },
        {
            "id": "6f922915-ce7e-492c-afaf-eba1477ac401",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4fdd9e4a-ceab-46ca-906c-5a617ffe0774"
        },
        {
            "id": "71ea6905-ea0e-4a2f-a23e-09948bfd82e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 7,
            "m_owner": "4fdd9e4a-ceab-46ca-906c-5a617ffe0774"
        },
        {
            "id": "b414ea9d-cd9d-47b0-b88f-f2ac81f0b611",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e76ee4bb-f216-4d80-b751-a4926fce5d5f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "4fdd9e4a-ceab-46ca-906c-5a617ffe0774"
        },
        {
            "id": "8c1441f3-ab18-4a2f-bb2c-ef0a32be1c95",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "4fdd9e4a-ceab-46ca-906c-5a617ffe0774"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a9874479-f166-466e-b984-a4e7d0758998",
    "visible": true
}