{
    "id": "18ffd353-0e65-447d-b0e4-e0f4718667de",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_playerdeath",
    "eventList": [
        {
            "id": "1d16a182-ae8c-4cb7-95a0-99f5c4b30d10",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "18ffd353-0e65-447d-b0e4-e0f4718667de"
        },
        {
            "id": "69ef0040-9895-4e71-a4bc-422f59d27189",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "4dafb8de-74e4-4263-b79a-0f3d9b26d1ba",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "18ffd353-0e65-447d-b0e4-e0f4718667de"
        },
        {
            "id": "a0789139-d75e-4058-92b2-f4875671cc57",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "18ffd353-0e65-447d-b0e4-e0f4718667de"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a9874479-f166-466e-b984-a4e7d0758998",
    "visible": true
}