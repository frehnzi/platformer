{
    "id": "e214c6a1-057b-4386-b0f6-44dc0803557c",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_demo",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Consolas",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "d75a82b3-a902-4730-91e3-96aca0a9b1fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 206,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "bdc5ee6b-644c-4725-a858-75280bb44de0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 22,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 232,
                "y": 74
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "b8f9d97c-e45c-480b-8eda-d04e86492704",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 142,
                "y": 74
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "07d2e06b-af4a-4b16-9da5-b39a872471a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 28,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "b0304644-0cf5-4e66-9028-5260ac64213a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 24,
                "y": 50
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "1b1606ef-07de-43f5-aaba-92df2d0a47bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "87b26155-0f88-4667-bcce-35c0c82f410f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "9fb1572d-f856-4dfc-8244-f164f6c9093c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 22,
                "offset": 4,
                "shift": 10,
                "w": 3,
                "x": 250,
                "y": 74
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "34bb9833-adc1-415d-ae13-a46ca0d5249c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 160,
                "y": 74
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "88f0eaac-6eea-4375-ab07-cd441fabbab6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 187,
                "y": 74
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "33b450f3-9fdc-4066-82bf-9bcf07f81fad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 211,
                "y": 50
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "1a40a1b2-53d8-4071-a8f5-59f9007d2eab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 110,
                "y": 26
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "f142bd7f-6448-4066-8620-7c4c23677844",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 211,
                "y": 74
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "3f4b232c-f47e-4be9-8614-9667dc952d4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 203,
                "y": 74
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "a3a59195-673a-469a-b8a7-508b663818eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 22,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 238,
                "y": 74
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "1686a45a-7500-4bb8-9bfa-00b212da35a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 92,
                "y": 74
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "59f7a6bb-807b-492f-82e9-6cf6d13a94e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 86,
                "y": 26
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "7186d5aa-807a-4fb3-9b75-2dc2d86ca8e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 133,
                "y": 26
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "86d18947-4776-4625-9ef2-055a3f5e97da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 57,
                "y": 50
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "867e0b15-30d5-48b5-8caf-ffe4564aec73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 101,
                "y": 50
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "a6a852b5-0019-4f07-a263-b63f963414db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 98,
                "y": 26
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "9495f45c-7e2f-4a46-9dba-cc15eccbab68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 178,
                "y": 50
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "a4500961-be7f-46cf-a288-df6c17ad5fb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 145,
                "y": 50
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "6dc77e6c-78cf-47ac-b155-3d7c79052b06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 134,
                "y": 50
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "20472494-a0d0-43d8-a47d-139036c89b8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 166,
                "y": 26
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "9118f25f-9948-42bb-a884-efd0d7a6dd2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 50,
                "y": 26
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "c8730d3f-59cb-48db-83fe-0a265acb2b92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 22,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 244,
                "y": 74
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "3158ecbb-e820-47e9-b166-fa1a3fb59c28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 225,
                "y": 74
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "0056987a-0bdf-4d78-b901-004dc2436e03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 62,
                "y": 74
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "6592490f-bc88-4759-945a-3cd20efd89ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 156,
                "y": 50
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "2002a1fa-0d11-4501-a148-d2148a93b76b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 112,
                "y": 50
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "50e28663-580a-4283-8924-2bf465b9ab9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 178,
                "y": 74
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "7a814a20-0f0f-40b7-a67f-7facd6935a34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "d15a179f-8e4e-4d63-97a4-c941d586fe59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 15,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "b05d3bc5-d8e1-43a9-92eb-eaf6c84eb110",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 79,
                "y": 50
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "3a4a0d18-fade-4e47-822c-2718cffe5b06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 158,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "ac8749d6-f2ed-4c91-a41f-011f987525ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 167,
                "y": 50
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "8688ffc0-bc74-4a02-b63b-58b0e1139e9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 122,
                "y": 74
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "46176825-3410-4505-966b-da592e0e6709",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 42,
                "y": 74
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "91fa552d-7348-4a40-9e63-8cee20a703f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 170,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "4a59ef5f-87e7-4917-828b-1ebdc94a538b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 200,
                "y": 50
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "1deabcb4-295f-480d-8f2d-a290c5191211",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 72,
                "y": 74
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "0a27b1b5-54d6-4583-87db-4b755f038841",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 102,
                "y": 74
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "317d1462-336e-42e6-b097-0fed3f0bfb86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 123,
                "y": 50
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "790e3292-a6bd-44b8-88b9-83056ae11a58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 112,
                "y": 74
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "1668c648-2ff7-4ed9-8359-78d459d02235",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 182,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "f73dbfaa-f5a3-48bc-ba51-86a3218a7bfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 90,
                "y": 50
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "f288777e-9fb9-4767-8932-24c16325aba5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 62,
                "y": 26
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "f8fda329-069e-4760-bbe0-0586bd1da8c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 189,
                "y": 50
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "a9d12030-e1ab-4b5a-94b3-c4b5b8972e71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "8b219c24-f696-4de3-ad71-dd29adfad8ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 46,
                "y": 50
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "b865260e-b33c-43f4-9b3d-e20818162a12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 122,
                "y": 26
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "d22e76e1-5573-40f1-86e7-824ae1f3f8da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 194,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "ee9bf082-9c22-4886-9577-4f96c016015a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 144,
                "y": 26
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "c6fefd16-c2ec-461e-b90e-21c22d5ffe88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 93,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "d054a92a-55bd-466b-8613-e6bd5dc6ea83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 145,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "fce77051-490d-4f31-9a61-9189c4506afb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 132,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "eb662aff-3027-443c-9ad3-5a34ef7fc514",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 119,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "b6ccee96-2672-4a2b-b97d-3a9d7f44f351",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 155,
                "y": 26
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "c7390de2-b10e-4b30-88fd-22b7eef1f9c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 22,
                "offset": 3,
                "shift": 10,
                "w": 5,
                "x": 218,
                "y": 74
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "776752e2-d2f3-41b6-a333-e4579a558a7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 68,
                "y": 50
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "70112f42-f647-4299-9066-486452aae787",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 195,
                "y": 74
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "2cacc503-6b1a-41b8-b8e4-3ea544f605e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 177,
                "y": 26
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "35616912-cc5f-465b-a631-227de5281d0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "5e8f4042-5eba-440d-a54f-c567ea808ac9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 7,
                "x": 169,
                "y": 74
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "5d2ff348-4797-402a-9e32-3b977c55cea2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 22,
                "y": 74
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "1d3b22d1-52e9-4ef8-93cf-bf49b5633ef9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 188,
                "y": 26
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "b000f600-aef1-4136-b6d4-7f469b702215",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "aefa33a1-e23c-4ba6-b7ba-ae48f6d11a37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 221,
                "y": 50
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "70159f42-2b28-4690-a2e6-1a6564966917",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 199,
                "y": 26
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "0f070378-2a0b-4fd2-bc16-654f29fa3103",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 38,
                "y": 26
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "593c573b-e444-4bfa-8c52-8b2f43ceea1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 218,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "06b4e5a4-fd7b-42d4-8734-535b62404b47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 241,
                "y": 50
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "1744efc2-4a40-4af7-88d2-990711ffa728",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 210,
                "y": 26
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "a89451ed-5250-444d-9d21-83e98a3b4102",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 231,
                "y": 50
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "15aa5d6b-cd6e-48de-92ec-a8bf1e36aa52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 221,
                "y": 26
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "d6c9af60-c416-4e0f-bd59-713a174127a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 232,
                "y": 26
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "8b9620ad-bfd0-4adf-a976-258f927764c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 230,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "e2b6c036-44ef-44f9-a2de-f5890b9167eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 32,
                "y": 74
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "5cf3d098-32b5-4627-92c6-1c9a5ad29e41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 242,
                "y": 2
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "d69e828d-ba9c-44cf-b2fb-5367085430c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 243,
                "y": 26
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "421e7adf-9d40-4a41-ac5a-5528a3fe6353",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 132,
                "y": 74
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "2a06d99b-8c31-4bf2-9505-c4dedd85daeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "1caf7cb8-c46e-4793-b952-9922259ef128",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 12,
                "y": 74
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "0490177f-db21-41d3-b32c-2dbbd00513a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 13,
                "y": 50
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "2c3ee0aa-79da-4eb0-8233-cbab6f50a650",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 52,
                "y": 74
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "ee727046-9141-4cc2-8a13-d6d8554219cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 26
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "67e00510-cab7-4e25-bda5-6a3cceaf83ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "cb1347d7-54d5-4be1-9868-81d6adabac7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 14,
                "y": 26
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "7b22a72a-5acb-40bf-8f2a-e3ea4fa562b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 26,
                "y": 26
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "f4a86339-4865-419a-a740-8e91a6378e76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 35,
                "y": 50
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "93c9a244-81fb-4d3a-bfe9-a70578d88744",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 82,
                "y": 74
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "3e785992-ae08-4441-b625-2030a6be33e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 22,
                "offset": 4,
                "shift": 10,
                "w": 2,
                "x": 2,
                "y": 98
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "d3814b63-951a-4988-bd6b-50f6f8ac4eef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 151,
                "y": 74
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "b16d1841-4e20-4ce7-9e46-908084237d1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 74,
                "y": 26
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 14,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}