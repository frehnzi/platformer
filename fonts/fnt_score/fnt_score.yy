{
    "id": "b046b06d-5e53-40b3-b943-aca81405374b",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_score",
    "AntiAlias": 1,
    "TTFName": "fonts\\fnt_score\\agency-fb-bold_[allfont.net].ttf",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Agency FB",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "b603ee4c-f6b9-4a09-b315-6065fb87f110",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 118,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 91,
                "y": 122
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "661ad496-7e70-4b77-9cd5-329938f68e93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 118,
                "offset": 4,
                "shift": 30,
                "w": 21,
                "x": 68,
                "y": 122
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "b16954e7-45ab-494b-95f0-78425be43ab5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 118,
                "offset": 6,
                "shift": 43,
                "w": 32,
                "x": 38,
                "y": 2
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "03c61812-4636-4bcf-87ed-03ff3fdb5afc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 118,
                "offset": 3,
                "shift": 20,
                "w": 11,
                "x": 113,
                "y": 122
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "4e4c042a-4fff-442b-b151-6c7f6c3a6b1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 118,
                "offset": 4,
                "shift": 39,
                "w": 31,
                "x": 140,
                "y": 2
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "9600c9bc-f9c6-4901-a7b1-0d5e3d65f94b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 118,
                "offset": 6,
                "shift": 42,
                "w": 31,
                "x": 173,
                "y": 2
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "11084032-fa75-41b5-98c9-10c8f356c5eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 118,
                "offset": 2,
                "shift": 38,
                "w": 34,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "c9520da0-2318-43f7-93aa-67fcf79a3cd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 118,
                "offset": 5,
                "shift": 41,
                "w": 31,
                "x": 206,
                "y": 2
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "8ce3663c-7afa-49ad-9ea0-173f7bb3c92e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 118,
                "offset": 6,
                "shift": 42,
                "w": 31,
                "x": 2,
                "y": 122
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "1724069b-10b0-4ea2-bd8a-e7c31412b5c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 118,
                "offset": 3,
                "shift": 36,
                "w": 31,
                "x": 35,
                "y": 122
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "b03e6226-bce5-43e1-80b8-54a706855fec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 118,
                "offset": 5,
                "shift": 43,
                "w": 32,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "bd4bf8ec-87d7-4eec-9270-93b7fbc51a9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 118,
                "offset": 5,
                "shift": 42,
                "w": 32,
                "x": 106,
                "y": 2
            }
        }
    ],
    "image": null,
    "includeTTF": true,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 45,
            "y": 45
        },
        {
            "x": 48,
            "y": 57
        }
    ],
    "sampleText": "5-7",
    "size": 75,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}