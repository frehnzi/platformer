//Figure out what player this is
if object_get_name(object_index) == "obj_player"
{
	game_player = 1;
	enemy_player = 2;
}
else if object_get_name(object_index) == "obj_player1"
{
	game_player = 2;
	enemy_player = 1;
}

//Player variables init
hspeed_ = 0; //current horizontal speed
vspeed_ = 0; //current vertical speed

max_strafe_speed = 6; //max horizontal speed while strafing in air
max_walking_speed = 4; //max horizontal speed while strafing on ground
max_glide_speed = 8; //max vertical speed while gliding
max_fall_speed = 16; //max vertical speed while falling
max_jump_speed = 8; //max vertical speed while jumping

walking_accel = .4; //horizontal speed boost per step
glide_accel = 0.4; //glide boost per step
fall_accel = 0.8; //fall boost per step
jump_accel = 0.2; //vertical jump boost per step

air_friction = 0.02; //decrement of horizontal speed per step while in air
ground_friction = 0.2; //decrement of horizontal speed per step while on ground

jump_init = 8; //initial vertical jump boost
hjump_boost = 4; //horizontal jump boost

jump_max = 8; //max amount of steps user can be vertically jumping
jump_current = 0; //current amount of steps user has been vertically jumping
max_jumps = 2;
current_jumps = 0;

//gravity selections
zero_grav = 0; 
reg_grav = 0.4; //DEFAULT

dead = false;
death_offset = 1;

if (game_player == 1)
{
	global.p1startx = x;
	global.p1starty = y;
}

if (game_player == 2)
{
	global.p2startx = x;
	global.p2starty = y;
}