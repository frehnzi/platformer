if (totalSpeed - death_offset > other.totalSpeed) && (other.dead == false) && (totalSpeed > 2)
{
	other.dead = true
	
	hit_sound = irandom_range(1,5);
	
	if (game_player == 1)
	{
		x = global.p1startx;
		y = global.p1starty;
	}
	if (game_player == 2)
	{
		x = global.p2startx;
		y = global.p2starty;
	}
	
	switch (hit_sound) {
	    case 1:
	        audio_play_sound(snd_hit1,1,false);
	        break;
		case 2:
	        audio_play_sound(snd_hit2,1,false);
	        break;
		case 3:
	        audio_play_sound(snd_hit3,1,false);
	        break;
		case 4:
	        audio_play_sound(snd_hit4,1,false);
	        break;
		case 5:
	        audio_play_sound(snd_hit5,1,false);
	        break;
	    default:
	        audio_play_sound(snd_hit1,1,false);
	        break;
	}
	
	if (game_player == 1)
	{
		obj_controller.p1_points += 1;
	}
	else if (game_player == 2)
	{
		obj_controller.p2_points += 1;
	}
}



