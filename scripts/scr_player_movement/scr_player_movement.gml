//Player horizontal input and movement
var allowAirAccel = true; //allow player to add to horizontal speed while in air

if (game_player == 2) //test if object is player 1 or 2 and adjust controls accordingly
{
	var hinput = keyboard_check(vk_right) - keyboard_check(vk_left); //1=Right -1=Left 0=NoInput
}
else if (game_player == 1)
{
	var hinput = keyboard_check(ord("D")) - keyboard_check(ord("A")); //^^^^^^^^^^^^^^^^^^^^^^^^
}

if (leftPressed) || (rightPressed) //allow player to instantly change horizontal direction
{
	hspeed_ = 0;
}

if (hinput != 0) && (!place_meeting(x,y+1, obj_solid)) //controls player input horizontal movement in air
{
	if (hspeed_ > max_walking_speed) && (hinput == -1) || (hspeed_ < -max_walking_speed) && (hinput == 1) //makes sure the player wont accel past max h-speed in air
	{
		allowAirAccel = false;
	}
	else allowAirAccel = true;
	
	hspeed_ += (hinput*walking_accel)*allowAirAccel; //accels player hinput direction in the air if they are allowed to
	
	if (vspeed_ > 0) //allow player to move faster horizontally in the air only while falling
	{
		hspeed_ = clamp(hspeed_, -max_strafe_speed, max_strafe_speed); 
	}
	else if (vspeed_ <= 0) //caps player h-speed to normal while jumping
	{
		hspeed_ = clamp(hspeed_, -max_walking_speed, max_walking_speed);
	}
}
else if (hinput != 0) && (place_meeting(x,y+1, obj_solid)) //controls player input horizontal movement on ground
{
	hspeed_ += hinput*walking_accel; //accels player hinput direction
	hspeed_ = clamp(hspeed_, -max_walking_speed, max_walking_speed); //cap player h-speed
}

if (!place_meeting(x,y+1, obj_solid)) && (hinput == 0) //in air friction implementation
{
	hspeed_ = lerp(hspeed_, 0, air_friction);
}
else if (place_meeting(x,y+1, obj_solid)) && (hinput == 0) //on ground friction implementation
{
	hspeed_ = lerp(hspeed_, 0, ground_friction);
}



//Player vertical input and movement

if (down) //falling
{
	vspeed_ += fall_accel;
	vspeed_ = clamp(vspeed_, -max_jump_speed, max_fall_speed);
}
else if (!up) && (!down) //gliding
{
	glide_accel = reg_grav;
	vspeed_ += glide_accel;
	vspeed_ = clamp(vspeed_, -max_jump_speed, max_glide_speed);
}

if (upPressed) && (current_jumps < max_jumps) //initial jump boost
{
	vspeed_ = 0; //resets jump var to init
	vspeed_ += -jump_init;
	current_jumps++;
	audio_play_sound(snd_jump1,1,false);
	if (upPressed) && (current_jumps == 1)
	{
		audio_play_sound(snd_jump2,1,false);
	}
}
else if (up)//allow player to jump higher based on how long jump input is held
{
	if (jump_current <= jump_max) //jump length timer
	{
		glide_accel = zero_grav; //dont turn on grav while jumping
		vspeed_ += -jump_accel;
		vspeed_ = clamp(vspeed_, -max_jump_speed, max_glide_speed);
		jump_current += 1;
	}
	else if (jump_current > jump_max) //stop jumping
	{
		glide_accel = reg_grav;
		vspeed_ += glide_accel;
		vspeed_ = clamp(vspeed_, -max_jump_speed, max_glide_speed);
	}
}
else if (upReleased) //reset jump hold length
{
	jump_current = 0;
}


if (left) && (upPressed) && (current_jumps < max_jumps) //gives player a small horizontal boost when moving and jumping in a direction
{
	hspeed_ = 0;
	hspeed_ += -hjump_boost;
}
else if (right) && (upPressed) && (current_jumps < max_jumps)
{
	hspeed_ = 0;
	hspeed_ += hjump_boost;
}


//Check if on ground
if (place_meeting(x,y+1, obj_solid)) && (!upPressed)
{
	current_jumps = 0;
}