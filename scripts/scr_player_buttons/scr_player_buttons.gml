if (game_player == 2)
{
	left = keyboard_check(vk_left);
	leftPressed = keyboard_check_pressed(vk_left);
	leftReleased = keyboard_check_released(vk_left);
	
	right = keyboard_check(vk_right);
	rightPressed = keyboard_check_pressed(vk_right);
	rightReleased = keyboard_check_released(vk_right);
	
	down = keyboard_check(vk_down);
	downPressed = keyboard_check_pressed(vk_down);
	downReleased = keyboard_check_released(vk_down);

	up = keyboard_check(vk_up);
	upPressed = keyboard_check_pressed(vk_up);
	upReleased = keyboard_check_released(vk_up);
}
else if (game_player == 1)
{
	left = keyboard_check(ord("A"));
	leftPressed = keyboard_check_pressed(ord("A"));
	leftReleased = keyboard_check_released(ord("A"));
	
	right = keyboard_check(ord("D"));
	rightPressed = keyboard_check_pressed(ord("D"));
	rightReleased = keyboard_check_released(ord("D"));
	
	down = keyboard_check(ord("S"));
	downPressed = keyboard_check_pressed(ord("S"));
	downReleased = keyboard_check_released(ord("S"));

	up = keyboard_check(ord("W"));
	upPressed = keyboard_check_pressed(ord("W"));
	upReleased = keyboard_check_released(ord("W"));
}