//Get button input
scr_player_buttons();

//player movement input
scr_player_movement();

//collision checking
scr_player_collision_check();

//make player move horizontally
x += hspeed_;

//make player move vertically
y += vspeed_;

if (dead)
{
	scr_player_death();
}

totalSpeed = (abs(hspeed_) + abs(vspeed_))/2

if (game_player == 1)
{
	global.p1totalSpeed = (abs(hspeed_) + abs(vspeed_))/2
}
else if (game_player == 2)
{
	global.p2totalSpeed = (abs(hspeed_) + abs(vspeed_))/2
}