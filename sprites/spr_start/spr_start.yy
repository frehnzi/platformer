{
    "id": "2a43fa5a-7bd3-43de-a875-dd55f945e97e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_start",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 174,
    "bbox_left": 160,
    "bbox_right": 423,
    "bbox_top": 145,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ffdab984-1559-455f-9e1b-4c9ee21534a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a43fa5a-7bd3-43de-a875-dd55f945e97e",
            "compositeImage": {
                "id": "054e63c1-16d0-4d6a-be2b-ba25175a0565",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffdab984-1559-455f-9e1b-4c9ee21534a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4acbf47e-42db-4598-bc44-ff56b8c7b49e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffdab984-1559-455f-9e1b-4c9ee21534a5",
                    "LayerId": "c020b0d5-50ab-4f57-9b4d-6fbc04706aaa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 330,
    "layers": [
        {
            "id": "c020b0d5-50ab-4f57-9b4d-6fbc04706aaa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a43fa5a-7bd3-43de-a875-dd55f945e97e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 525,
    "xorig": 275,
    "yorig": 161
}