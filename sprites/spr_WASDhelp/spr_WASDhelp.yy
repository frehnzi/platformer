{
    "id": "b12ea6f2-b2f8-44f0-9e46-86e943a84094",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_WASDhelp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 262,
    "bbox_left": 123,
    "bbox_right": 415,
    "bbox_top": 58,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b5f17ce4-8afe-4116-a4d4-b320f3737a51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b12ea6f2-b2f8-44f0-9e46-86e943a84094",
            "compositeImage": {
                "id": "ad06cf91-0c99-4f32-a523-3b5161d96d56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5f17ce4-8afe-4116-a4d4-b320f3737a51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "675ac12a-7167-49b5-ace1-772a93f8ba56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5f17ce4-8afe-4116-a4d4-b320f3737a51",
                    "LayerId": "50bcafa8-2291-48c4-adc2-6a1b5a68e9a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 330,
    "layers": [
        {
            "id": "50bcafa8-2291-48c4-adc2-6a1b5a68e9a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b12ea6f2-b2f8-44f0-9e46-86e943a84094",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 525,
    "xorig": 269,
    "yorig": 176
}