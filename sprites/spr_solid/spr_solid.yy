{
    "id": "43fcafb8-4fa5-4d07-854b-d1f716f28ec6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_solid",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "80339300-289e-447e-ad57-40dc6825f2e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43fcafb8-4fa5-4d07-854b-d1f716f28ec6",
            "compositeImage": {
                "id": "ac852855-d08f-4c61-8a14-81f799eb1e72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80339300-289e-447e-ad57-40dc6825f2e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39ed30e9-fd3a-4c3b-a0c4-fb1da2333891",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80339300-289e-447e-ad57-40dc6825f2e5",
                    "LayerId": "b2e24caa-ab9b-4e6f-ae4e-01c8d81af840"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b2e24caa-ab9b-4e6f-ae4e-01c8d81af840",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "43fcafb8-4fa5-4d07-854b-d1f716f28ec6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}