{
    "id": "fdae3fae-e36a-4691-bb78-f74dcc9b8be2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c79863f4-1365-4bd5-a72a-b5e93b03f110",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdae3fae-e36a-4691-bb78-f74dcc9b8be2",
            "compositeImage": {
                "id": "b1219a06-6bfd-441b-acb8-1187549ab7be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c79863f4-1365-4bd5-a72a-b5e93b03f110",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dda990b3-5b2b-48a0-aa93-93c89857215b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c79863f4-1365-4bd5-a72a-b5e93b03f110",
                    "LayerId": "7b649908-2d0e-46fc-b92c-ae2f437ae131"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7b649908-2d0e-46fc-b92c-ae2f437ae131",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fdae3fae-e36a-4691-bb78-f74dcc9b8be2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}