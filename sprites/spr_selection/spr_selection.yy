{
    "id": "8d7c34fc-9812-450e-ac86-df46467d9895",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_selection",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 799,
    "bbox_left": 0,
    "bbox_right": 1310,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6483a843-cb38-4a04-9171-823f24444738",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d7c34fc-9812-450e-ac86-df46467d9895",
            "compositeImage": {
                "id": "ac10aa7e-8352-49e4-addd-62dd22f1b3ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6483a843-cb38-4a04-9171-823f24444738",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1822ad3a-d10b-4ddc-a27f-8e8ce55f74bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6483a843-cb38-4a04-9171-823f24444738",
                    "LayerId": "f6c9358c-05cf-449c-9de4-44d4ff499a30"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 800,
    "layers": [
        {
            "id": "f6c9358c-05cf-449c-9de4-44d4ff499a30",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8d7c34fc-9812-450e-ac86-df46467d9895",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1311,
    "xorig": 655,
    "yorig": 400
}