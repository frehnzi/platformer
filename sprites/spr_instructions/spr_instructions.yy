{
    "id": "76f88e66-2d47-422f-8c43-3abc67aa2cc1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_instructions",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 124,
    "bbox_left": 10,
    "bbox_right": 340,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ca5e2515-bf8b-4fd5-9e8c-5cad43b71f9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76f88e66-2d47-422f-8c43-3abc67aa2cc1",
            "compositeImage": {
                "id": "15f5eaf2-cf0a-4968-b058-d512e659ddf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca5e2515-bf8b-4fd5-9e8c-5cad43b71f9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c5e300e-eddc-40f0-bebb-b66b0db6cc1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca5e2515-bf8b-4fd5-9e8c-5cad43b71f9a",
                    "LayerId": "419e7777-09ee-4a5c-a3ed-e8998823fea8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 165,
    "layers": [
        {
            "id": "419e7777-09ee-4a5c-a3ed-e8998823fea8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "76f88e66-2d47-422f-8c43-3abc67aa2cc1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 378,
    "xorig": 0,
    "yorig": 0
}