{
    "id": "40d65e0f-a903-47cd-8148-eaeed087dadf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_arena3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 766,
    "bbox_left": 0,
    "bbox_right": 1279,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "35eb2093-6dc7-4a20-a5a6-fca064b47898",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40d65e0f-a903-47cd-8148-eaeed087dadf",
            "compositeImage": {
                "id": "bccc028b-4bef-4211-b260-58f6d4d8dc8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35eb2093-6dc7-4a20-a5a6-fca064b47898",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffbba137-3190-4a0c-953e-bb3629979f42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35eb2093-6dc7-4a20-a5a6-fca064b47898",
                    "LayerId": "48a034fb-9cd6-4a3d-b958-25f346a3c109"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 767,
    "layers": [
        {
            "id": "48a034fb-9cd6-4a3d-b958-25f346a3c109",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "40d65e0f-a903-47cd-8148-eaeed087dadf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 640,
    "yorig": 383
}