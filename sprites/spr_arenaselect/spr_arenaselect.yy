{
    "id": "d44be0fc-4f24-4028-b43d-9abfa6c1da25",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_arenaselect",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 75,
    "bbox_left": 145,
    "bbox_right": 353,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e9496384-7f4b-4347-88ae-83255195b5b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d44be0fc-4f24-4028-b43d-9abfa6c1da25",
            "compositeImage": {
                "id": "e8039350-7e7e-40af-8cbb-e99d06d3180d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9496384-7f4b-4347-88ae-83255195b5b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ba46bef-f58d-4670-970a-b0d8d439256e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9496384-7f4b-4347-88ae-83255195b5b2",
                    "LayerId": "5d2cbc0f-fb42-41f6-aae5-c4b32043ec62"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "5d2cbc0f-fb42-41f6-aae5-c4b32043ec62",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d44be0fc-4f24-4028-b43d-9abfa6c1da25",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 500,
    "xorig": 0,
    "yorig": 0
}