{
    "id": "58c73656-ac3f-471b-b7bc-fa6518281fc9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_arena2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 768,
    "bbox_left": 0,
    "bbox_right": 1279,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1e76400e-8ef2-4084-b38b-c6556b3386a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58c73656-ac3f-471b-b7bc-fa6518281fc9",
            "compositeImage": {
                "id": "e7cfdbae-22da-483f-8499-9c04be0b3b19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e76400e-8ef2-4084-b38b-c6556b3386a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6dea532-8597-4aed-8744-46cb9971437e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e76400e-8ef2-4084-b38b-c6556b3386a3",
                    "LayerId": "60dd969c-6400-46ac-b639-bc563f811996"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 769,
    "layers": [
        {
            "id": "60dd969c-6400-46ac-b639-bc563f811996",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "58c73656-ac3f-471b-b7bc-fa6518281fc9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 640,
    "yorig": 384
}