{
    "id": "a9874479-f166-466e-b984-a4e7d0758998",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2d4d4ad0-4505-449f-a9d5-a5458923d058",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9874479-f166-466e-b984-a4e7d0758998",
            "compositeImage": {
                "id": "a28942cf-83eb-4452-a9ce-18f8d1017b96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d4d4ad0-4505-449f-a9d5-a5458923d058",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2fb8cab-099d-4bb1-849b-a04b26493cf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d4d4ad0-4505-449f-a9d5-a5458923d058",
                    "LayerId": "abf15f33-5c3e-4af3-9447-da0e7a209e86"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "abf15f33-5c3e-4af3-9447-da0e7a209e86",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a9874479-f166-466e-b984-a4e7d0758998",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}