{
    "id": "abf9805a-f470-408a-9d49-dc1be10360c4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_title",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 147,
    "bbox_left": 101,
    "bbox_right": 474,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "04581eef-4109-4196-b248-326093021467",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "abf9805a-f470-408a-9d49-dc1be10360c4",
            "compositeImage": {
                "id": "8e2ed4c2-ae91-40c8-9265-afe58693a8ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04581eef-4109-4196-b248-326093021467",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38646e7c-66c6-4323-85e9-7b249909c4f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04581eef-4109-4196-b248-326093021467",
                    "LayerId": "db74bebe-c8f1-45f5-8265-ef7eb7dcea9b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 165,
    "layers": [
        {
            "id": "db74bebe-c8f1-45f5-8265-ef7eb7dcea9b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "abf9805a-f470-408a-9d49-dc1be10360c4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 525,
    "xorig": 278,
    "yorig": 78
}