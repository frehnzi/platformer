{
    "id": "efcdc07a-c52a-44e0-9b7c-880a059b63ab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_arrowhelp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 262,
    "bbox_left": 123,
    "bbox_right": 415,
    "bbox_top": 58,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "186aefba-66e5-4246-a37e-29c6d8a39f3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efcdc07a-c52a-44e0-9b7c-880a059b63ab",
            "compositeImage": {
                "id": "b6930b61-49de-4319-a0be-8f69a282a7b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "186aefba-66e5-4246-a37e-29c6d8a39f3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c85b2201-04fd-4c82-b0bb-75c78f3093c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "186aefba-66e5-4246-a37e-29c6d8a39f3a",
                    "LayerId": "d029ba7b-9cb7-475a-a5ef-95a34a2a0d81"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 330,
    "layers": [
        {
            "id": "d029ba7b-9cb7-475a-a5ef-95a34a2a0d81",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "efcdc07a-c52a-44e0-9b7c-880a059b63ab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 525,
    "xorig": 268,
    "yorig": 165
}