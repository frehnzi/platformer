{
    "id": "8e61c532-0151-4e08-8b76-9365b6cf6e3d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_redwin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 137,
    "bbox_left": 135,
    "bbox_right": 632,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "80ecbfb5-1a61-4555-88c1-d8b248e3d418",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e61c532-0151-4e08-8b76-9365b6cf6e3d",
            "compositeImage": {
                "id": "c9bc1d0f-d4cb-4385-b110-f33e255fc24b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80ecbfb5-1a61-4555-88c1-d8b248e3d418",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64c65f25-831f-4cdc-a921-8ad69ed5ba28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80ecbfb5-1a61-4555-88c1-d8b248e3d418",
                    "LayerId": "ad14d0c2-a10f-4a52-8fb0-5f7f1e92ba3a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "ad14d0c2-a10f-4a52-8fb0-5f7f1e92ba3a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8e61c532-0151-4e08-8b76-9365b6cf6e3d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 750,
    "xorig": 379,
    "yorig": 76
}