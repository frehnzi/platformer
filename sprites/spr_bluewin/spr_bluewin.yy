{
    "id": "02d51569-49d0-4090-9518-e4a5bc309aff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bluewin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 118,
    "bbox_left": 187,
    "bbox_right": 580,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3b3aba0b-93e5-457e-991f-daefcf5949f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02d51569-49d0-4090-9518-e4a5bc309aff",
            "compositeImage": {
                "id": "e95e3f10-2f0f-4708-8606-b2849f11eec8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b3aba0b-93e5-457e-991f-daefcf5949f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b858594-46f7-43f0-a4e4-c4abeaec6709",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b3aba0b-93e5-457e-991f-daefcf5949f9",
                    "LayerId": "69ba30d5-601f-4e93-b2a3-d466126d1b64"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "69ba30d5-601f-4e93-b2a3-d466126d1b64",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "02d51569-49d0-4090-9518-e4a5bc309aff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 750,
    "xorig": 367,
    "yorig": 72
}