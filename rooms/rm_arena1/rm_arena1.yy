
{
    "name": "rm_arena1",
    "id": "d3dca55f-99eb-4273-b455-5a933adbcb11",
    "creationCodeFile": "RoomCreationCode.gml",
    "inheritCode": false,
    "inheritCreationOrder": false,
    "inheritLayers": false,
    "instanceCreationOrderIDs": [
        "3d4dc1b6-a592-47b1-ac87-42b80def4277",
        "37232b8a-7dc6-4fc7-9139-66fe1f778956",
        "7d045795-6893-4c80-b5e6-06a0fc9dc766",
        "d86a45ff-b774-46ac-bdcf-8c7db74b42af",
        "766cf974-0664-472f-b76b-de5e9d62e900",
        "c755b00a-7eed-479f-8709-d671965f0c67",
        "a0a6b9d3-4ac1-4b54-aa0c-e8ba5e9f4750",
        "9692e13f-95e3-4aaa-b537-06b42574f6c6",
        "e75305a2-7608-4a08-ade2-393f56c1a1d0",
        "76d6e16e-ea97-4c61-b27e-dc38a166311f",
        "78897288-2ac5-4050-a82e-4a6d730df45c",
        "34e83b43-80fb-442f-8efa-c63699201b82",
        "6918d985-ea6b-4ce7-af78-914e8f124682",
        "437c8390-74d8-4e81-bf42-3fd3d490a366",
        "85077a7e-d8a9-4dcb-a012-9a0dbc632373",
        "b79592cb-e57f-4983-ad23-0461bd0647dd"
    ],
    "IsDnD": false,
    "layers": [
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Instances",
            "id": "47c6d65c-6c39-4c4f-8cff-365ae1f8ce68",
            "depth": 0,
            "grid_x": 16,
            "grid_y": 16,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_5C2B367C","id": "3d4dc1b6-a592-47b1-ac87-42b80def4277","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_5C2B367C","objId": "4fdd9e4a-ceab-46ca-906c-5a617ffe0774","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 112,"y": 208},
{"name": "inst_5ED51C34","id": "37232b8a-7dc6-4fc7-9139-66fe1f778956","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_5ED51C34","objId": "4dafb8de-74e4-4263-b79a-0f3d9b26d1ba","properties": null,"rotation": 0,"scaleX": 25,"scaleY": 1,"mvc": "1.0","x": 0,"y": 368},
{"name": "inst_42062A26","id": "7d045795-6893-4c80-b5e6-06a0fc9dc766","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_42062A26","objId": "4dafb8de-74e4-4263-b79a-0f3d9b26d1ba","properties": null,"rotation": 0,"scaleX": 10,"scaleY": 1,"mvc": "1.0","x": 240,"y": 176},
{"name": "inst_2D058590","id": "d86a45ff-b774-46ac-bdcf-8c7db74b42af","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_2D058590","objId": "4dafb8de-74e4-4263-b79a-0f3d9b26d1ba","properties": null,"rotation": 0,"scaleX": 3,"scaleY": 1,"mvc": "1.0","x": 96,"y": 240},
{"name": "inst_51BC6F26","id": "766cf974-0664-472f-b76b-de5e9d62e900","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_51BC6F26","objId": "4dafb8de-74e4-4263-b79a-0f3d9b26d1ba","properties": null,"rotation": 0,"scaleX": 2,"scaleY": 1,"mvc": "1.0","x": 464,"y": 96},
{"name": "inst_74905B93","id": "c755b00a-7eed-479f-8709-d671965f0c67","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_74905B93","objId": "4dafb8de-74e4-4263-b79a-0f3d9b26d1ba","properties": null,"rotation": 0,"scaleX": 2,"scaleY": 1,"mvc": "1.0","x": 144,"y": 96},
{"name": "inst_3376D16B","id": "a0a6b9d3-4ac1-4b54-aa0c-e8ba5e9f4750","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_3376D16B","objId": "4dafb8de-74e4-4263-b79a-0f3d9b26d1ba","properties": null,"rotation": 0,"scaleX": 16,"scaleY": 1,"mvc": "1.0","x": 384,"y": 368},
{"name": "inst_7771BF69","id": "85077a7e-d8a9-4dcb-a012-9a0dbc632373","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7771BF69","objId": "4dafb8de-74e4-4263-b79a-0f3d9b26d1ba","properties": null,"rotation": 0,"scaleX": 15,"scaleY": 1,"mvc": "1.0","x": 0,"y": 0},
{"name": "inst_7E618FD8","id": "9692e13f-95e3-4aaa-b537-06b42574f6c6","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7E618FD8","objId": "4dafb8de-74e4-4263-b79a-0f3d9b26d1ba","properties": null,"rotation": 0,"scaleX": 27.5,"scaleY": 1,"mvc": "1.0","x": 200,"y": 0},
{"name": "inst_441AE307","id": "e75305a2-7608-4a08-ade2-393f56c1a1d0","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_441AE307","objId": "4dafb8de-74e4-4263-b79a-0f3d9b26d1ba","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 22,"mvc": "1.0","x": 624,"y": 16},
{"name": "inst_C8F6618","id": "76d6e16e-ea97-4c61-b27e-dc38a166311f","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_C8F6618","objId": "4dafb8de-74e4-4263-b79a-0f3d9b26d1ba","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 22.5,"mvc": "1.0","x": 0,"y": 16},
{"name": "inst_44FC9736","id": "78897288-2ac5-4050-a82e-4a6d730df45c","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_44FC9736","objId": "4dafb8de-74e4-4263-b79a-0f3d9b26d1ba","properties": null,"rotation": 0,"scaleX": 2,"scaleY": 3.125,"mvc": "1.0","x": 304,"y": 272},
{"name": "inst_2764CD45","id": "34e83b43-80fb-442f-8efa-c63699201b82","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_2764CD45","objId": "e76ee4bb-f216-4d80-b751-a4926fce5d5f","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 512,"y": 208},
{"name": "inst_4CD184BC","id": "6918d985-ea6b-4ce7-af78-914e8f124682","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_4CD184BC","objId": "4dafb8de-74e4-4263-b79a-0f3d9b26d1ba","properties": null,"rotation": 0,"scaleX": 3,"scaleY": 1,"mvc": "1.0","x": 496,"y": 240},
{"name": "inst_2FF18FD0","id": "437c8390-74d8-4e81-bf42-3fd3d490a366","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_2FF18FD0","objId": "4dafb8de-74e4-4263-b79a-0f3d9b26d1ba","properties": null,"rotation": 0,"scaleX": 2,"scaleY": 3.8125,"mvc": "1.0","x": 304,"y": 128},
{"name": "inst_4BA32AF1","id": "b79592cb-e57f-4983-ad23-0461bd0647dd","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_4BA32AF1","objId": "4166f9d0-1f67-4b73-b246-484dbd5a0fcc","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 608,"y": 352}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Background",
            "id": "8945f976-8267-49f8-9a95-89a005bedb35",
            "animationFPS": 15,
            "animationSpeedType": "0",
            "colour": { "Value": 4279900698 },
            "depth": 100,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "hspeed": 0,
            "htiled": false,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "spriteId": "00000000-0000-0000-0000-000000000000",
            "stretch": false,
            "userdefined_animFPS": false,
            "userdefined_depth": false,
            "visible": true,
            "vspeed": 0,
            "vtiled": false,
            "x": 0,
            "y": 0
        }
    ],
    "modelName": "GMRoom",
    "parentId": "00000000-0000-0000-0000-000000000000",
    "physicsSettings":     {
        "id": "195e2173-39d4-43aa-a342-ebac857c831f",
        "inheritPhysicsSettings": false,
        "modelName": "GMRoomPhysicsSettings",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 10,
        "PhysicsWorldPixToMeters": 0.1,
        "mvc": "1.0"
    },
    "roomSettings":     {
        "id": "4f76e494-1cc2-4fe7-9080-685cb9f4caef",
        "Height": 384,
        "inheritRoomSettings": false,
        "modelName": "GMRoomSettings",
        "persistent": false,
        "mvc": "1.0",
        "Width": 640
    },
    "mvc": "1.0",
    "views": [
{"id": "c9291147-e2cb-4140-bf17-4b9bb8bd322a","hborder": 32,"hport": 768,"hspeed": -1,"hview": 384,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": true,"vspeed": -1,"wport": 1280,"wview": 640,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "94c7d732-92fc-48a9-8f82-dbb74c74334d","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "ea97e780-7662-44ac-8bda-c9363ed5d4e1","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "42a9af4f-d7e3-46d2-81ee-a7998eda4ff1","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "deff56e4-125b-4aee-b8a5-bb6cde97e459","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "3e5581ae-f6df-4f94-b23a-fa7373ca9818","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "5534139b-812d-49a8-a2a7-d6a4014bf3ce","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "b3523fe3-0d8d-4572-8b42-d6db6d8763f7","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0}
    ],
    "viewSettings":     {
        "id": "41a10fad-c00b-4512-8931-b1f26d5299bb",
        "clearDisplayBuffer": true,
        "clearViewBackground": false,
        "enableViews": true,
        "inheritViewSettings": false,
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0"
    }
}